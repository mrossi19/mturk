// ==UserScript==
// @name         John Doe
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  better defaults
// @author       You
// @match        https://www.google.com/evaluation/endor/mturk*
// @grant        GM_log
// @require      http://code.jquery.com/jquery-3.1.1.min.js
// ==/UserScript==

$(function(){
    GM_log('in fnction');
});

window.addEventListener ("keydown", kbhandler, false);

function kbhandler(event) {
    //GM_log('in kbhandler ' + event + ' ' + event.code);
    if (event.code == 'Enter') {
        $('input[class="submit-hit"]').click();
    } else if (event.code == 'KeyF') {
    } else if (event.code == 'KeyE') {
    }
}

$(document).ready(function(){
    $('textarea[name="comment"]').hide();
    $('h2').hide();
    $('h3').hide();
    $('#hidden-instructions').hide();

    $('#playable-div').html(
        '<select name="playback-status">' +
        '<option selected value="PLAYABLE">Playable</option>' +
        '<option value="NOT_PLAYABLE">Not playable</option>' +
        '</select>');

    $('#playable-div').append(
        '&nbsp;' +
        '<select name="speech-presence">' +
        '<option value="ENGLISH">Speech ENGLISH</option>' +
        '<option selected value="FOREIGN">Speech FOREIGN</option>' +
        '<option value="ENGLISH_AND_FOREIGN">Speech BOTH</option>' +
        '<option value="NO_LANGUAGE_PRESENT">Speech None</option>' +
        '</select>');

    $('#playable-div').append(
        '&nbsp;' +
        '<select name="text-presence">' +
        '<option value="ENGLISH">Text ENGLISH</option>' +
        '<option value="FOREIGN">Text FOREIGN</option>' +
        '<option value="ENGLISH_AND_FOREIGN">Text BOTH</option>' +
        '<option selected value="NO_LANGUAGE_PRESENT">Text None</option>' +
        '</select>');

    $('#playable-div').append(
        '&nbsp;' +
        '<select name="sensitivity">' +
        '<option value="SENSITIVE">Sensitive</option>' +
        '<option selected value="NOT_SENSITIVE">Not Sensitive</option>' +
        '<option value="MAYBE_SENSITIVE">Maybe Sensitive</option>' +
        '</select>');

    $('#playable-div').append(
        '<p>' +
        '<select multiple size=12 name="categories">' +
        '<option value="PROFANE_OR_OFFENSIVE_LANGUAGE">Inappropriate Language - Profane, sexual, hateful or offensive</option>' +
        '<option value="VIOLENCE">Violence - Real or video game violence, accidents, destruction, animal violence</option>' +
        '<option value="WEAPONS_OR_WAR">Weapons or War - inc in video games</option>' +
        '<option value="SEXUAL">Sexual behavior - incl suggestive dancing, kissing, fetish</option>' +
        '<option value="NUDITY">Nudity or near-nudity - incl tight bikinis, breast-feeding</option>' +
        '<option value="DRUGS">Drugs - (whether legal or illegal) or drug use</option>' +
        '<option value="HORROR_OR_FEAR">Horror or Fear - incl horror movies, scary pranks</option>' +
        '<option value="GORE_OR_GRAPHIC">Gore or Graphic - incl blood, injuries, med procedures</option>' +
        '<option value="ILLEGAL_OR_DISHONEST">Illegal or Dishonest behavior - incl stealing or cheating</option>' +
        '<option value="DANGEROUS">Dangerous behavior</option>' +
        '<option value="MORAL_ISSUES">Moral Issues - incl religious preaching, gambling</option>' +
        '<option value="OTHER_CATEGORY">Other</option>' +
        '</select>');

    $('#playable-div').append(
        '<select name="severity">'+
        '<option disabled selected value>Select Severity</option>' +
        '<option value="FEW">Few</option>' +
        '<option value="SOME">Some</option>' +
        '<option value="MANY">Many</option>' +
        '<option value="MOST">Most</option>' +
        '</select>');

    $('#playable-div').append(
        '&nbsp;' +
        '<select multiple size=5 name="medium">' +
        '<option name="SPEECH">Speech</option>' +
        '<option name="SOUNDS">Sounds</option>' +
        '<option name="TEXT_ON_SCREEN">Text on screen</option>' +
        '<option name="VISUALS">Visuals</option>' +
        '<option name="OTHER_MEDIUM">Other</option>' +
        '</select>');
});
